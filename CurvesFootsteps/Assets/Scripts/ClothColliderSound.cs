﻿using System;
using System.Collections;
using System.Collections.Generic;
using FMODUnity;
using UnityEngine;

public class ClothColliderSound : MonoBehaviour
{
    [FMODUnity.EventRef] public string ClothEvent;
    public Collider BodyCollider;
    public Animator PlayerAnimator;
   
    private void OnTriggerEnter(Collider other)
    {
        if (other == BodyCollider)
        {
            FMOD.Studio.EventInstance ClothInstance = FMODUnity.RuntimeManager.CreateInstance(ClothEvent);
            ClothInstance.setParameterByName("InputMagnitude", PlayerAnimator.GetFloat("InputMagnitude"));
            ClothInstance.set3DAttributes(RuntimeUtils.To3DAttributes(transform.position));
            ClothInstance.start();
            ClothInstance.release();
        }
    }
}
