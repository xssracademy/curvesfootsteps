﻿using System.Collections;
using System.Collections.Generic;
using FMODUnity;
using UnityEngine;

public class CurveFootsteps : MonoBehaviour
{
    [FMODUnity.EventRef] public string FootstepEvent;
    public Animator PlayerAnimator;
    public GameObject FootLeft;
    public GameObject FootRight;
    public bool RightIsPlaying;
    public bool LeftIsPlaying;
    
    public LayerMask lm;
    float surface;
   
    void Update()
    {
        if (PlayerAnimator.GetFloat("footLeft") > 0.01 && !LeftIsPlaying)
        {
            FMOD.Studio.EventInstance FootLeftInstance = FMODUnity.RuntimeManager.CreateInstance(FootstepEvent);
            SurfaceCheck(FootLeft);
            FootLeftInstance.set3DAttributes(RuntimeUtils.To3DAttributes(FootLeft));
            FootLeftInstance.setParameterByName("foot_velocity", PlayerAnimator.GetFloat("footLeft") * 100);
            FootLeftInstance.setParameterByName("InputMagnitude", PlayerAnimator.GetFloat("InputMagnitude"));
            FootLeftInstance.setParameterByName("surface", surface);
            FootLeftInstance.start();
            FootLeftInstance.release();
            LeftIsPlaying = true;
        }
        else if (PlayerAnimator.GetFloat("footLeft") == 0) LeftIsPlaying = false;
        
        
        if (PlayerAnimator.GetFloat("footRight") > 0.01 && !RightIsPlaying)
        {
            FMOD.Studio.EventInstance FootRightInstance = FMODUnity.RuntimeManager.CreateInstance(FootstepEvent);
            SurfaceCheck(FootRight);
            FootRightInstance.set3DAttributes(RuntimeUtils.To3DAttributes(FootRight));
            FootRightInstance.setParameterByName("foot_velocity", PlayerAnimator.GetFloat("footRight") * 100);
            FootRightInstance.setParameterByName("InputMagnitude", PlayerAnimator.GetFloat("InputMagnitude"));
            FootRightInstance.setParameterByName("surface", surface);
            FootRightInstance.start();
            FootRightInstance.release();
            RightIsPlaying = true;
        }
        else if (PlayerAnimator.GetFloat("footRight") == 0) RightIsPlaying = false;
    }

        void SurfaceCheck(GameObject leg)
        {

            if (Physics.Raycast(leg.transform.position, Vector3.down, out RaycastHit hit, 0.3f, lm))
            {
                switch (hit.collider.tag)
                {
                    case "Concrete":
                        surface = 0f;
                        break;
                    case "Grass":
                        surface = 1f;
                        break;
                    default:
                        surface = 0f;
                        break;
                }

            }
        }

}
